#!/usr/bin/env python3

from .Modules import *
from .Manager import *


class EntryTransaction:

    def __init__(self, node: ET.Element, manager):
        self.id = str()         # type: str
        self.name = str()       # type: str
        self.account = str()    # type: str
        self.amount = float()   # type: float
        self.ignored = False    # type: bool

        self._parse(node, manager)

    def _parse(self, node: ET.Element, manager):

        if "Id" not in node.attrib:
            err_msg = "Found a <{}> with no Id in an Entry."
            err_msg = err_msg.format(node.tag)
            raise ET.ParseError(err_msg)
        self.id = node.attrib["Id"]

        name_xml = node.findall("Name")
        if len(name_xml) != 1:
            err_msg = "Found a <{}> with {} <Name> in an Entry."
            err_msg = err_msg.format(node.tag, len(name_xml))
            raise ET.ParseError(err_msg)
        self.name = name_xml[0].text

        account_xml = node.findall("Account")
        if len(account_xml) > 1:
            err_msg = "Found a <{}> with {} <Account> in Regular.xml."
            err_msg = err_msg.format(node.tag, len(account_xml))
            raise ET.ParseError(err_msg)
        if len(account_xml) == 1:
            self.account = account_xml[0].text
        else:
            self.account = manager.default_account_id

        amount_xml = node.findall("Amount")
        if len(amount_xml) != 1:
            err_msg = "Found a <{}> with {} <Amount> in an Entry."
            err_msg = err_msg.format(node.tag, len(amount_xml))
            raise ET.ParseError(err_msg)
        self.amount = float(amount_xml[0].text)

        self.ignored = node.find("Ignored") is not None


class Income(EntryTransaction):

    def __init__(self, node: ET.Element, manager):
        super().__init__(node, manager)


class Debt(EntryTransaction):

    def __init__(self, node: ET.Element, manager):
        super().__init__(node, manager)


class Entry:

    def __init__(self, filename: str, manager):

        self.date = datetime.datetime.min   # type: datetime.datetime
        self.incomes = list()               # type: list[Income]
        self.debts = list()                 # type: list[Debt]

        self._parse(filename, manager)

    def _parse(self, filename: str, manager):

        root = ET.parse(filename).getroot()

        self.date = datetime.datetime.fromisoformat(os.path.basename(filename)[:-4])

        accounts_xml = root.findall("Accounts")
        if len(accounts_xml) != 1:
            err_msg = "Found {} <Accounts> in " + filename + "."
            err_msg = err_msg.format(len(accounts_xml))
            raise ET.ParseError(err_msg)
        self._parse_accounts(accounts_xml[0], manager)

        incomes_xml = root.findall("Incomes")
        if len(incomes_xml) != 1:
            err_msg = "Found {} <Incomes> in " + filename + "."
            err_msg = err_msg.format(len(incomes_xml))
            raise ET.ParseError(err_msg)
        self._parse_incomes(incomes_xml[0], manager)

        debts_xml = root.findall("Debts")
        if len(debts_xml) != 1:
            err_msg = "Found {} <Debts> in " + filename + "."
            err_msg = err_msg.format(len(debts_xml))
            raise ET.ParseError(err_msg)
        self._parse_debts(debts_xml[0], manager)

    def _parse_accounts(self, node: ET.Element, manager):

        id_list = list()
        for account_xml in node.findall("Account"):
            id_list.append(self._parse_account(account_xml, manager))

        for key in manager.accounts:
            if key not in id_list:
                manager.accounts[key].amounts[self.date] = 0.0

    def _parse_account(self, node: ET.Element, manager):

        if "Id" not in node.attrib:
            err_msg = "Found an <Account> with no Id in an Entry."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)

        if "Amount" not in node.attrib:
            err_msg = "Found an <Account> with no Amount in an Entry."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)

        manager.accounts[node.attrib["Id"]].amounts[self.date] = float(node.attrib["Amount"])

        return node.attrib["Id"]

    def _parse_incomes(self, node: ET.Element, manager):

        incomes_xml = node.findall("Income")
        for income_xml in incomes_xml:
            self.incomes.append(Income(income_xml, manager))

        monthly_incomes_xml = node.findall("MonthlyIncome")
        id_list = list()
        for monthly_income_xml in monthly_incomes_xml:
            id_list.append(self._parse_monthly_income(monthly_income_xml, manager))

        for key in manager.monthly_incomes:
            if key not in id_list:
                manager.monthly_incomes[key].amounts[self.date] = 0.0
                manager.monthly_incomes[key].paids[self.date] = True

    def _parse_monthly_income(self, node: ET.Element, manager):

        if "Id" not in node.attrib:
            err_msg = "Found a <MonthlyIncome> with no Id in an Entry."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)
        if node.attrib["Id"] not in manager.monthly_incomes:
            err_msg = "Found a <MonthlyIncome> with an Id not recognized in an Entry. Id: {}"
            err_msg = err_msg.format(node.attrib["Id"])
            raise ET.ParseError(err_msg)

        if "Amount" not in node.attrib:
            err_msg = "Found a <MonthlyIncome> with no Amount in an Entry."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)
        manager.monthly_incomes[node.attrib["Id"]].amounts[self.date] = float(node.attrib["Amount"])

        if "Paid" not in node.attrib:
            err_msg = "Found a <MonthlyIncome> with no Paid in an Entry."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)
        if node.attrib["Paid"] == "True":
            paid = True
        elif node.attrib["Paid"] == "False":
            paid = False
        else:
            err_msg = "Found a <MonthlyIncome> with an unrecognized Paid in an Entry. Paid: '{}'."
            err_msg = err_msg.format(node.attrib["Paid"])
            raise ET.ParseError(err_msg)
        manager.monthly_incomes[node.attrib["Id"]].paids[self.date] = paid

        return node.attrib["Id"]

    def _parse_debts(self, node: ET.Element, manager):

        debts_xml = node.findall("Debt")
        for debt_xml in debts_xml:
            self.debts.append(Debt(debt_xml, manager))

        monthly_debts_xml = node.findall("MonthlyDebt")
        id_list = list()
        for monthly_debt_xml in monthly_debts_xml:
            id_list.append(self._parse_monthly_debt(monthly_debt_xml, manager))

        for key in manager.monthly_debts:
            if key not in id_list:
                manager.monthly_debts[key].amounts[self.date] = 0.0
                manager.monthly_debts[key].paids[self.date] = True

    def _parse_monthly_debt(self, node: ET.Element, manager):

        if "Id" not in node.attrib:
            err_msg = "Found a <MonthlyDebt> with no Id in an Entry."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)
        if node.attrib["Id"] not in manager.monthly_debts:
            err_msg = "Found a <MonthlyDebt> with an Id not recognized in an Entry. Id: {}"
            err_msg = err_msg.format(node.attrib["Id"])
            raise ET.ParseError(err_msg)

        if "Amount" not in node.attrib:
            err_msg = "Found a <MonthlyDebt> with no Amount in an Entry."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)
        manager.monthly_debts[node.attrib["Id"]].amounts[self.date] = float(node.attrib["Amount"])

        if "Paid" not in node.attrib:
            err_msg = "Found a <MonthlyDebt> with no Paid in an Entry."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)
        if node.attrib["Paid"] == "True":
            paid = True
        elif node.attrib["Paid"] == "False":
            paid = False
        else:
            err_msg = "Found a <MonthlyDebt> with an unrecognized Paid in an Entry. Paid: '{}'."
            err_msg = err_msg.format(node.attrib["Paid"])
            raise ET.ParseError(err_msg)
        manager.monthly_debts[node.attrib["Id"]].paids[self.date] = paid

        return node.attrib["Id"]

    def gross_worth(self, manager, accounts=tuple()):

        s = 0
        for account in accounts:
            s += manager.accounts[account].amounts[self.date]

        return s

    def net_worth(self, manager, accounts=tuple()):

        s = 0
        for account in accounts:
            s += manager.accounts[account].amounts[self.date]

        for income in self.incomes:
            if income.account in accounts and not income.ignored:
                s += income.amount

        for debt in self.debts:
            if debt.account in accounts and not debt.ignored:
                s -= debt.amount

        for income in manager.monthly_incomes:
            if manager.monthly_incomes[income].account in accounts and not manager.monthly_incomes[income].paids[self.date]:
                s += manager.monthly_incomes[income].amounts[self.date]

        for debt in manager.monthly_debts:
            if manager.monthly_debts[debt].account in accounts and not manager.monthly_debts[debt].paids[self.date]:
                s -= manager.monthly_debts[debt].amounts[self.date]

        return s

    def pessimistic_worth(self, manager, accounts=tuple()):

        s = 0
        for account in accounts:
            s += manager.accounts[account].amounts[self.date]

        for debt in self.debts:
            if debt.account in accounts and not debt.ignored:
                s -= debt.amount

        for debt in manager.monthly_debts:
            if manager.monthly_debts[debt].account in accounts and not manager.monthly_debts[debt].paids[self.date]:
                s -= manager.monthly_debts[debt].amounts[self.date]

        return s

    def optimistic_worth(self, manager, accounts=tuple()):

        s = 0
        for account in accounts:
            s += manager.accounts[account].amounts[self.date]

        for income in self.incomes:
            if income.account in accounts and not income.ignored:
                s += income.amount

        for income in manager.monthly_incomes:
            if manager.monthly_incomes[income].account in accounts and not manager.monthly_incomes[income].paids[self.date]:
                s += manager.monthly_incomes[income].amounts[self.date]

        return s
