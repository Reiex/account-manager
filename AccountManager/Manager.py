#!/usr/bin/env python3

from .Modules import *
from .Entry import *
from .LogBook import *


class Account:

    def __init__(self, index: int, node: ET.Element):
        self.id = str()             # type: str
        self.name = str()           # type: str
        self.default = False        # type: bool
        self.amounts = LogBook()    # type: LogBook[float]
        self.index = index          # type: int

        self._parse(node)

    def _parse(self, node: ET.Element):

        if "Id" not in node.attrib:
            err_msg = "Found an <Account> with no Id in Accounts.xml."
            err_msg = err_msg.format()
            raise ET.ParseError(err_msg)
        self.id = node.attrib["Id"]

        name_xml = node.findall("Name")
        if len(name_xml) != 1:
            err_msg = "Found an <Account> with {} <Name> in Accounts.xml."
            err_msg = err_msg.format(len(name_xml))
            raise ET.ParseError(err_msg)
        self.name = name_xml[0].text

        if node.find("Default"):
            self.default = True


class RegularTransaction:

    def __init__(self, node: ET.Element, manager):
        self.id = str()             # type: str
        self.name = str()           # type: str
        self.account = str()        # type: str
        self.amounts = LogBook()    # type: LogBook[float]
        self.paids = LogBook()      # type: LogBook[bool]

        self._parse(node, manager)

    def _parse(self, node: ET.Element, manager):

        if "Id" not in node.attrib:
            err_msg = "Found a <{}> with no Id in Regular.xml."
            err_msg = err_msg.format(node.tag)
            raise ET.ParseError(err_msg)
        self.id = node.attrib["Id"]

        name_xml = node.findall("Name")
        if len(name_xml) != 1:
            err_msg = "Found a <{}> with {} <Name> in Regular.xml."
            err_msg = err_msg.format(node.tag, len(name_xml))
            raise ET.ParseError(err_msg)
        self.name = name_xml[0].text

        account_xml = node.findall("Account")
        if len(account_xml) > 1:
            err_msg = "Found a <{}> with {} <Account> in Regular.xml."
            err_msg = err_msg.format(node.tag, len(account_xml))
            raise ET.ParseError(err_msg)
        if len(account_xml) == 1:
            self.account = account_xml[0].text
        else:
            self.account = manager.default_account_id


class MonthlyIncome(RegularTransaction):

    def __init__(self, node: ET.Element, manager):
        super().__init__(node, manager)


class MonthlyDebt(RegularTransaction):

    def __init__(self, node: ET.Element, manager):
        super().__init__(node, manager)


class Manager:

    def __init__(self, path: str):

        self.accounts = dict()              # type: dict[str, Account]
        self.default_account_id = str()     # type: str
        self.monthly_incomes = dict()       # type: dict[str, MonthlyIncome]
        self.monthly_debts = dict()         # type: dict[str, MonthlyDebt]
        self.entries = LogBook()            # type: LogBook[Entry]

        self._init(path)

    def _init(self, path: str):

        self._parse_accounts(os.path.join(path, "Accounts.xml"))
        self._parse_regulars(os.path.join(path, "Regular.xml"))

        path_entries = os.path.join(path, "Entries")
        for filename in os.listdir(path_entries):
            print(filename)
            entry = Entry(os.path.join(path_entries, filename), self)
            self.entries[entry.date] = entry

    def _parse_accounts(self, filename: str):

        root = ET.parse(filename).getroot()

        accounts_xml = root.findall("Account")
        for a, account_xml in enumerate(accounts_xml):
            account = Account(a, account_xml)
            self.accounts[account.id] = account

        default_count = 0
        for key in self.accounts:
            if self.accounts[key].default:
                default_count += 1
                self.default_account_id = key

        if default_count > 1:
            err_msg = "Found {} <Account> with <Default/> in Accounts.xml."
            err_msg = err_msg.format(default_count)
            raise ET.ParseError(err_msg)

        if default_count == 0:
            self.accounts[accounts_xml[0].attrib["Id"]].default = True
            self.default_account_id = accounts_xml[0].attrib["Id"]

    def _parse_regulars(self, filename: str):

        root = ET.parse(filename).getroot()

        monthly_incomes_xml = root.findall("MonthlyIncome")
        for monthly_income_xml in monthly_incomes_xml:
            monthly_income = MonthlyIncome(monthly_income_xml, self)
            self.monthly_incomes[monthly_income.id] = monthly_income

        monthly_debts_xml = root.findall("MonthlyDebt")
        for monthly_debt_xml in monthly_debts_xml:
            monthly_debt = MonthlyDebt(monthly_debt_xml, self)
            self.monthly_debts[monthly_debt.id] = monthly_debt

    def get_ordered_accounts(self):
        return [x for x in sorted(self.accounts, key=lambda key: self.accounts[key].index)]

    def gross_worth(self, accounts: typing.Union[typing.Tuple[str], str] = tuple()):

        if isinstance(accounts, str):
            accounts = [accounts]
        elif len(accounts) == 0:
            accounts = self.accounts.keys()

        book = LogBook()
        for date, entry in self.entries:
            book[date] = entry.gross_worth(self, accounts)

        return book

    def net_worth(self, accounts: typing.Union[typing.Tuple[str], str] = tuple()):

        if isinstance(accounts, str):
            accounts = [accounts]
        elif len(accounts) == 0:
            accounts = self.accounts.keys()

        book = LogBook()
        for date, entry in self.entries:
            book[date] = entry.net_worth(self, accounts)

        return book

    def pessimistic_worth(self, accounts: typing.Union[typing.Tuple[str], str] = tuple()):

        if isinstance(accounts, str):
            accounts = [accounts]
        elif len(accounts) == 0:
            accounts = self.accounts.keys()

        book = LogBook()
        for date, entry in self.entries:
            book[date] = entry.pessimistic_worth(self, accounts)

        return book

    def optimistic_worth(self, accounts: typing.Union[typing.Tuple[str], str] = tuple()):

        if isinstance(accounts, str):
            accounts = [accounts]
        elif len(accounts) == 0:
            accounts = self.accounts.keys()

        book = LogBook()
        for date, entry in self.entries:
            book[date] = entry.optimistic_worth(self, accounts)

        return book
