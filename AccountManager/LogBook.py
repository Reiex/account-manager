#!/usr/bin/env python3

from .Modules import *


T = typing.TypeVar('T')


class LogBook(typing.Generic[T]):

    def __init__(self):

        self._values = list()  # type: list[typing.Tuple[datetime.datetime, T]]

    def __iter__(self):
        return iter(self._values)

    @staticmethod
    def fusion_dates(book_a, book_b):

        dates = set()
        for date, _ in book_a:
            dates.add(date)
        for date, _ in book_b:
            dates.add(date)

        return dates

    def __radd__(self, other):

        result = LogBook()

        for date, value in self._values:
            result[date] = self[date] + other

        return result

    def __add__(self, book):

        result = LogBook()

        for date in LogBook.fusion_dates(self, book):
            result[date] = self[date] + book[date]

        return result

    def __sub__(self, book):

        result = LogBook()

        for date in LogBook.fusion_dates(self, book):
            result[date] = self[date] - book[date]

        return result

    def __mul__(self, book):

        result = LogBook()

        for date in LogBook.fusion_dates(self, book):
            result[date] = self[date] * book[date]

        return result

    def __truediv__(self, book):

        result = LogBook()

        for date in LogBook.fusion_dates(self, book):
            result[date] = self[date] / book[date]

        return result

    def __getitem__(self, key: datetime.datetime) -> T:

        if len(self._values) == 0:
            return 0.0

        if key < self._values[0][0]:
            return 0.0

        i = 1
        while i < len(self._values):
            if self._values[i - 1][0] <= key < self._values[i][0]:
                return self._values[i - 1][1]
            i += 1

        return self._values[-1][1]

    def __setitem__(self, key: datetime.datetime, value: T):

        if len(self._values) == 0:
            self._values.append((key, value))
            return None

        if key < self._values[0][0]:
            self._values.insert(0, (key, value))
            return None

        i = 1
        while i < len(self._values):
            if self._values[i - 1][0] <= key < self._values[i][0]:
                if key == self._values[i - 1][0]:
                    self._values[i - 1] = (key, value)
                else:
                    self._values.insert(i, (key, value))
                return None
            i += 1

        self._values.append((key, value))

    def __str__(self):

        result = "{ (LOGBOOK)\n"
        for date, value in self._values:
            result += date.isoformat() + ": " + str(value) + "\n"
        result += "}"

        return result

    def plot(self, color: str = "", style: str = ""):

        dates = matplotlib.dates.date2num([x[0] for x in self._values])
        if color and not style:
            plt.plot_date(dates, [x[1] for x in self._values], "-", color=color, linewidth=2.5)
        elif style and not color:
            plt.plot_date(dates, [x[1] for x in self._values], style, linewidth=2.5)
        elif not style and not color:
            plt.plot_date(dates, [x[1] for x in self._values], "-", linewidth=2.5)
        else:
            plt.plot_date(dates, [x[1] for x in self._values], style=style, color=color, linewidth=2.5)
