#!/usr/bin/env python3

from matplotlib.dates import date2num
import matplotlib.pyplot as plt
import datetime
import sys

import AccountManager as AM

ACCOUNTS = list()


def plot_economies(manager):

    gross = manager.gross_worth()
    net_stable = manager.net_worth(ACCOUNTS[2:])

    now = datetime.datetime.now()

    print("""Economies:
    - Total brut: {}€
    - Total net stable: {}€
    """.format(gross[now], net_stable[now]))

    gross.plot("#004586")
    net_stable.plot("#FF4000")

    plt.show()

    livrets = manager.net_worth(ACCOUNTS[2:8])
    epargnes = manager.net_worth(ACCOUNTS[8:])

    print("""Livrets/AV:
    - Livrets: {}€
    - Assurances Vie et autres epargnes: {}€
    """.format(livrets[now], epargnes[now]))

    epargnes.plot("#004586")
    livrets.plot("#FF4000")

    plt.show()


def plot_budget(manager, y_range, days):

    pessimistic = manager.pessimistic_worth(ACCOUNTS[0])
    gross = manager.gross_worth(ACCOUNTS[0])
    net = manager.net_worth(ACCOUNTS[0])

    now = datetime.datetime.now()

    print("""Budget:
    - Fin de mois: {}€
    - CCP: {}€
    - Dispo: {}€
    """.format(net[now], gross[now], pessimistic[now]))

    pessimistic.plot("#004586")
    gross.plot("#FFBF00")
    net.plot("#FF4000")

    before = now - datetime.timedelta(days=days)
    plt.xlim(date2num(before), date2num(now))
    plt.ylim(y_range[0], y_range[1])
    plt.show()


def main(args):

    global ACCOUNTS

    manager = AM.Manager(args[1])
    ACCOUNTS = manager.get_ordered_accounts()

    plot_economies(manager)
    plot_budget(manager, [0, 1000], 200)

    # sum(income.amounts for income in manager.monthly_incomes.values()).plot(color="#FF4000")
    # sum(debt.amounts for debt in manager.monthly_debts.values()).plot()
    # plt.show()


if __name__ == "__main__":
    main(sys.argv)
